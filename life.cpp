#include <iostream>
#include <unistd.h>
#include <math.h>
#include <sstream>
using namespace std;

//n is the size of one side of the square
int n = 20;

bool check(int* current,int i, int j){

	int dead = 0;
	int alive = 0;
	//top left
	if(i == 0 && j == 0){
		for(int k = 0; k < 2; k++){
			for(int y = 0;y < 2; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0)
			return true;
		return false;
	}
	//top right
	if(i == 0 && j == n-1){
		for(int k = 0; k < 2; k++){
			for(int y = -1;y < 1; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0)
			return true;
		return false;
	}
	//bottom left
	if(i == n-1 && j == 0){
		for(int k = -1; k < 1; k++){
			for(int y = 0;y < 2; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0)
			return true;
		return false;
	}
	//bottom right
	if(i == n-1 && j == n-1){
		for(int k = -1; k < 1; k++){
			for(int y = -1;y < 1; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0)
			return true;
		return false;
	}
	//top row
	if(i==0){
		for(int k = 0; k < 1; k++){
			for(int y = -1;y < 2; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0 && alive < 4)
			return true;
		return false;
	}
	//bottom row
	if(i==n-1){
		for(int k = -1; k < 1; k++){
			for(int y = -1;y < 2; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0 && alive < 4)
			return true;
		return false;
	}
	//left col
	if(j==0){
		for(int k = -1; k < 2; k++){
			for(int y = 0;y < 2; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0 && alive < 4)
			return true;
		return false;
	}
	//right col
	if(j==n-1){
		for(int k = -1; k < 2; k++){
			for(int y = -1;y < 1; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0 && alive < 4)
			return true;
		return false;
	}
	//everyother case
	else{
		for(int k = -1; k < 2; k++){
			for(int y = -1;y < 2; y++){
				if(*(current+(n*(i+k)+(j+y)))==0)
					dead++;
				else
					alive++;
			}

		}
		if(alive > 0 && alive < 4)
			return true;
		return false;
	}
}

//create next map of 
void generation(int* current, int* next){
	for(int i = 0; i < n;i++){
		for(int j = 0; j < n;j++){
			if(check(current, i, j))
				*(next+(n*i+j)) = 1;
			else
				*(next+(n*i+j)) = 0;
		}
	}
	//updates current
	for(int i = 0; i < n;i++){
		for(int j = 0; j < n;j++){
			*(current+ (n*i+j)) = *(current + (n*i+j));
		}
	}

}


//make a line
string makeLine(){
	string line = "_";
	for(int i = 0; i<n-1;i++){
		line+="_";
	}
	line+="\n";
	return line;
}

//print out map
void display(int* current){
	stringstream ss;
	ss << makeLine();
	for(int i = 0; i < n;i++){
		for(int j = 0; j < n;j++){
			ss << *(current + (n*i+j));
			if(j == n-1){
				ss << "\n";
			}
		}
	}
	string s = ss.str();
	cout << s << endl;
}


int main(){
	int* current = new int[400];
	int* next = new int[400];
	for(int i = 0; i < n*n; i++){
			*(current + i) = 0;
			*(next + i) = 0;
	}

	//choices for map
	int choice;
	cout << "choose one of the follwing: \n";
	cout << "1: Glider, Beehive \n";
	cout << "2: Lightwweight spaceship \n";
	cout << "3: Pulsar \n\n";
	cin >> choice;	

	if (choice == 1){
		//Glider
		*(current+ 2) = 1;
		*(current+ n) = 1;
		*(current+ (n+2)) = 1;
		*(current+ (n*2+1)) = 1;
		*(current+ (n*2+1)) = 1;
		*(current+ (n*2+2)) = 1;

		//Beehive
		*(current+ (n*5+1)) = 1;
		*(current+ (n*5+2)) = 1;
		*(current+ (n*6)) = 1;
		*(current+ (n*6+3)) =1;
		*(current+ (n*7+1)) =1;
		*(current+ (n*7+2)) = 1;
	}			
	else if(choice == 2){
		//lightweight spaceship
		*current = 1;
		*(current + 3) = 1;
		*(current + (n+4)) = 1;
		*(current + (n*2)) = 1;
		*(current + (n*2+4)) = 1;
		*(current + (n*3+1)) = 1;
		*(current + (n*3+2)) = 1;
		*(current + (n*3+3)) = 1;
		*(current + (n*3+4)) = 1;
	}
	else{
		//pulsar top half
		*(current + 2) = 1;
		*(current + 3) = 1;
		*(current + 4) = 1;
		*(current + 8) = 1;
		*(current + 9) = 1;
		*(current + 10) = 1;

		*(current + (n*2)) = 1;
		*(current + (n*2+5)) = 1;
		*(current + (n*2+7)) = 1;
		*(current + (n*2+12)) = 1;

		*(current + (n*3)) =1;
		*(current + (n*3+5)) = 1;
		*(current + (n*3+7)) = 1;
		*(current + (n*3+12)) = 1;

		*(current + (n*4)) = 1;
		*(current + (n*4+5)) = 1;
		*(current + (n*4+7)) = 1;
		*(current + (n*4+12)) = 1;

		*(current + (n*5+2)) = 1;
		*(current + (n*5+3)) = 1;
		*(current + (n*5+4)) = 1;
		*(current + (n*5+8)) = 1;
		*(current + (n*5+9))  = 1;
		*(current + (n*5+10)) = 1;
		
		//pulsar bottom half
		*(current + (n*7+2)) = 1;
		*(current + (n*7+3)) = 1;
		*(current + (n*7+4)) = 1;
		*(current + (n*7+8)) = 1;
		*(current + (n*7+9)) = 1;
		*(current + (n*7+10)) = 1;

		*(current + (n*9)) = 1;
		*(current + (n*9+5)) = 1;
		*(current + (n*9+7)) = 1;
		*(current + (n*9+12)) = 1;

		*(current + (n*10)) =1;
		*(current + (n*10+5)) = 1;
		*(current + (n*10+7)) = 1;
		*(current + (n*10+12)) = 1;

		*(current + (n*11)) = 1;
		*(current + (n*11+5)) = 1;
		*(current + (n*11+7)) = 1;
		*(current + (n*11+12)) = 1;

		*(current + (n*12+2)) = 1;
		*(current + (n*12+3)) = 1;
		*(current + (n*12+4)) = 1;
		*(current + (n*12+8)) = 1;
		*(current + (n*12+9)) = 1;
		*(current + (n*12+10)) = 1;

		
	}

	while(true){
		int count = 0;
		generation(current,next);
		display(next);
		usleep(800000);
		if(count == n)
			break;
	}
	display(current);
	delete current;
	delete next;

}
